### phase_2.mk --- 
## 
## Filename: phase_2.mk
## Description: 
## Author: Michele
## Maintainer: 
## Created: Mon Jan 28 13:47:02 2013 (+0100)
## Version: 
## Last-Updated: 
##           By: 
##     Update #: 0
## URL: 
## Keywords: 
## Compatibility: 
## 
######################################################################
## 
### Commentary: 
## 
## 
## 
######################################################################
## 
### Change Log:
## 
## 
######################################################################
## 
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License as
## published by the Free Software Foundation; either version 3, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
## General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program; see the file COPYING.  If not, write to
## the Free Software Foundation, Inc., 51 Franklin Street, Fifth
## Floor, Boston, MA 02110-1301, USA.
## 
######################################################################
## 
### Code:

# mitocondrial genes found, via blast, in contigs of third assembly step
extern ../../../../mito/dataset/ploidy_test/phase_1/bestHitSortedBySubject.tab as MITO_GENES

extern ../phase_1/step3_info_contigreadlist_count as ALLELES_COUNT


# links
mito_genes.fasta:
	ln -sf $(MITO_GENES) $@

alleles_count.tab:
	ln -sf $(ALLELES_COUNT) $@


alleles_mito_count: mito_genes.fasta alleles_count.tab
	translate -a -f 1 <(sed '1d' <$^2) 4 <$< \
	| grep --invert-match -e "tRNA" -e "ribosomal" > $@   * remove tRNA and rRNA *


alleles_mito_count_trna_mito: mito_genes.fasta alleles_count.tab
	translate -a -f 1 <(sed '1d' <$^2) 4 <$< >$@


alleles_mito_count_stat: alleles_mito_count
	cut -f 4-8  $< | bsort --key=1,1 | uniq -f 1 \
	| stat_base -p 2 --median --mean --stdev --mean-stdev 2 3 4 5 >$@


alleles_mito_count_stat_trna_mito_stat: alleles_mito_count_trna_mito
	cut -f 4-8  $< | bsort --key=1,1 | uniq -f 1 \
	| stat_base -p 2 --median --mean --stdev --mean-stdev 2 3 4 5 >$@



alleles_mito_count_trna_mito: mito_genes.fasta alleles_count.tab
	translate -a -f 1 <(sed '1d' <$^2) 4 <$< >$@


alleles_mito_count_boxplot.pdf: alleles_mito_count
	cut -f 4-8  $< | bsort --key=1,1 | uniq -f 1 \
	| boxplot -r -e -o $@





######################################################################
### phase_2.mk ends here

# Copyright 2014 Michele Vidotto <michele.vidotto@gmail.com>

context prj/454_preprocessing

PRJ_AS ?= test1

STRAINS ?= 4

GENOME1 ?= individual_1
GENOME2 ?= individual_2
GENOME3 ?= individual_3
GENOME4 ?= individual_4

LB1 ?= HAPLO1
LB2 ?= HAPLO2
LB3 ?= HAPLO3
LB4 ?= HAPLO4



# LB1 tetraploid
extern ../../../../genome_simulation/dataset/$(GENOME1)/phase_1/individual.fastq as STRAIN1_FASTQ
extern ../../../../genome_simulation/dataset/$(GENOME1)/phase_1/individual.strain as STRAIN1_STRAIN
# extern ../../../../454_preprocessing/dataset/$(GENOME1)/phase_2/cleaned.xml as STRAIN1_XML

# LB2 tetraploid
extern ../../../../genome_simulation/dataset/$(GENOME2)/phase_1/individual.fastq as STRAIN2_FASTQ
extern ../../../../genome_simulation/dataset/$(GENOME2)/phase_1/individual.strain as STRAIN2_STRAIN
# extern ../../../../454_preprocessing/dataset/$(GENOME2)/phase_2/cleaned.xml as STRAIN2_XML

# LB3 diploid
extern ../../../../genome_simulation/dataset/$(GENOME3)/phase_1/individual.fastq as STRAIN3_FASTQ
extern ../../../../genome_simulation/dataset/$(GENOME3)/phase_1/individual.strain as STRAIN3_STRAIN
# extern ../../../../454_preprocessing/dataset/$(GENOME3)/phase_2/cleaned.xml as STRAIN3_XML

# LB4 diploid
extern ../../../../genome_simulation/dataset/$(GENOME4)/phase_1/individual.fastq as STRAIN4_FASTQ
extern ../../../../genome_simulation/dataset/$(GENOME4)/phase_1/individual.strain as STRAIN4_STRAIN
# extern ../../../../454_preprocessing/dataset/$(GENOME4)/phase_2/cleaned.xml as STRAIN4_XML




# link original data in fasta format
.SECONDEXPANSION:
strain%.fastq: $$(STRAIN%_FASTQ)
	ln -sf $< $@

strain%.strain: $$(STRAIN%_STRAIN)
	ln -sf $< $@

strain%.xml: $$(STRAIN%_XML)
	ln -sf $< $@


# prepare MIRA input files
$(PRJ_AS)_in.454.fastq: $(addsuffix .fastq, $(addprefix strain, $(shell seq -s " " $(STRAINS)))) # strain1.fastq strain2.fastq strain3.fastq strain4.fastq ..
	cat $^ > $@

.IGNORE: $(PRJ_AS)_straindata_in.txt # straindata file not always necessary
$(PRJ_AS)_straindata_in.txt: $(addsuffix .strain, $(addprefix strain, $(shell seq -s " " $(STRAINS)))) # strain1.strain strain2.strain strain3.strain strain4.strain
	cat $^ > $@

.IGNORE: $(PRJ_AS)_traceinfo_in.454.xml # straindata file not always necessary
$(PRJ_AS)_traceinfo_in.454.xml: $(addsuffix .xml, $(addprefix strain, $(shell seq -s " " $(STRAINS)))) # strain1.xml strain2.xml
	merge_trace_xmls $^ -o $@





# avoid parallel execution targets
.NOTPARALLEL: step1_assembly.log step2_assembly.log step3_assembly.log


# assembly
.PRECIOUS: step1_assembly.log
step1_assembly.log: $(PRJ_AS)_in.454.fastq $(PRJ_AS)_straindata_in.txt
	!threads
	$(call load_modules); \
	$(call step1_assembly_param, $@) \
	2>&1 \
	| tee $1


.PRECIOUS: step2_assembly.log
step2_assembly.log: step1_assembly.log
	!threads
	$(call load_modules); \
	$(call step2_assembly_param, $@) \
	2>&1 \
	| tee $1


.PRECIOUS: step3_assembly.log
step3_assembly.log: step2_assembly.log
	!threads
	$(call load_modules); \
	$(call step3_assembly_param, $@) \
	2>&1 \
	| tee $1



%_info_contigreadlist_collapsed: %_assembly.log
	bawk '!/^[$$,\#+]/ { \
	print $$0; \
	}' < $(basename $<)/$*_d_info/$*_info_contigreadlist.txt \
	| bsort --key=1,1 --key=2,2 \
	| set_collapse 2 -s -o \
	> $@


%_allele_count: %_info_contigreadlist_collapsed
	bawk 'BEGIN { print "# sequence","$(LB1)","$(LB2)","$(LB3)","$(LB4)","remain"  } \
	!/^[$$,\#+]/ { \
	strain1=0; strain2=0; strain3=0; strain4=0; remain=0;\
	printf "%s\t",$$1; \
	split($$2,reads,";"); \
	for (i in reads) \
	{ \
	if (reads[i] ~ /$(LB1)/) {strain1++}; \
	if (reads[i] ~ /$(LB2)/) {strain2++}; \
	if (reads[i] ~ /$(LB3)/) {strain3++}; \
	if (reads[i] ~ /$(LB4)/) {strain4++}; \
	if (reads[i] ~ /remain/) {remain++}; \
	}; \
	printf "%i\t%i\t%i\t%i\t%i\n",strain1,strain2,strain3,strain4,remain; \
	}' <$< >$@


%_allele_count_boxplot.pdf: %_allele_count
	cut -f 6 --complement $< \
	| boxplot -r -o $@

%_allele_count_histo.pdf: %_allele_count
	distrib_plot -t histogram --remove-na -b 75 -o $@ 2 3 4 5 <$<

# custom
%_allele_count_histo_lim.pdf: %_allele_count
	this_distrib_plot -r -t histogram -b 75 -o $@ 2 3 4 5 <$<


%_allele_shared: %_allele_count
	bawk 'BEGIN { print "# sequence","$(LB1)","$(LB2)","$(LB3)","$(LB4)","remain"  } \
	!/^[$$,\#+]/ { \
	if ( $$2 > 0 && $$3 > 0 && $$4 > 0 && $$5 > 0 ) \
	{ \
	print $$0; \
	} \
	}' <$< >$@


%_allele_double: %_allele_count
	bawk 'BEGIN { print "# sequence","$(LB1)","$(LB2)","$(LB3)","$(LB4)","remain"  } \
	!/^[$$,\#+]/ { \
	if ( ( $$4 >= 2*$$2 && $$5 >= 2*$$3 ) && ( $$4 >= 2*$$3 && $$5 >= 2*$$2 ) ) \
	{ \
	print $$0; \
	} \
	}' <$< >$@


%_allele_shared_double: %_allele_count
	bawk 'BEGIN { print "# sequence","$(LB1)","$(LB2)","$(LB4)","$(LB5)","remain"  } \
	!/^[$$,\#+]/ { \
	if ( ( $$4 >= 2*$$2 && $$5 >= 2*$$3 ) && ( $$4 >= 2*$$3 && $$5 >= 2*$$2 ) ) \
	{ \
	print $$0; \
	} \
	}' <$< >$@



%_allele_max: %_allele_count
	bawk 'BEGIN { print "# sequence","$(LB1)-$(LB2)","$(LB3)-$(LB4)"  } \
	function max(x){i=0;for(val in x){if(i<=x[val]){i=x[val];}}return i;} \
	function min(x){i=max(x);for(val in x){if(i>x[val]){i=x[val];}}return i;} \
	!/^[$$,\#+]/ { \
	a[1]=$$2; a[2]=$$3; b[1]=$$4; b[2]=$$5; \
	print $$1, max(a), max(b); \
	}' <$< > $@


%_allele_max_histo.pdf: %_allele_max
	distrib_plot -r -t histogram -b 75 -o $@ 2 3 <$<


%_allele_ratio: %_allele_max
	bawk 'BEGIN { print "# sequence","$(LB1)-$(LB2)","$(LB3)-$(LB4)"; up1=0; low1=0; eq1=0;  } \
	!/^[$$,\#+]/ { \
	if ( $$2 > 0 && $$3 > 0 ) { \
	print $$1, $$3/$$2; \
	} \
	}' <$< > $@


%_allele_bins: %_allele_max
	>$@; \
	for BIN in {1..8}; do \
	bawk -v bin=$$BIN 'BEGIN { print "# bin",">"bin,"<"bin,"="bin; up1=0; low1=0; eq1=0;  } \
	!/^[$$,\#+]/ { \
	if ( $$2 > 0 && $$3 > 0 ) { \
	ratio=($$3)/($$2); \
	a=bin; b=1/a; \
	if ( ratio > a ) {up1++;} \
	if ( ratio < b ) {low1++;} \
	if ( ratio >= b && ratio <= a ) {eq1++;} \
	} \
	} END { print bin,up1,low1,eq1;  }' <$< >> $@; \
	done


%_allele_ratio_boxplot.pdf: %_allele_ratio
	sed '1d' $< | boxplot -e -r -o $@







.PHONY: test
test:
	@echo $(454_INPUT)
	$(addsuffix .strain, $(addprefix strain, $(shell seq -s " " $(STRAINS))))


# This should be the default target.
ALL   += $(PRJ_AS)_in.454.fastq \
	 $(PRJ_AS)_straindata_in.txt \
	 $(PRJ_AS)_traceinfo_in.454.xml \
	 step1_assembly.log \
	 step2_assembly.log \
	 step3_assembly.log \
	 step3_info_contigreadlist_collapsed \
	 step3_allele_count \
	 step3_allele_count_histo.pdf \
	 step3_allele_count_histo_lim.pdf \
	 step3_allele_shared \
	 step3_allele_double \
	 step3_allele_max \
	 step3_allele_ratio \
	 step3_allele_bins \
	 step3_allele_ratio_boxplot.pdf


INTERMEDIATE +=

CLEAN += $(addsuffix .fastq, $(addprefix strain, $(shell seq -s " " $(STRAINS)))) \
	 $(addsuffix .strain, $(addprefix strain, $(shell seq -s " " $(STRAINS)))) \
	 $(addsuffix .xml, $(addprefix strain, $(shell seq -s " " $(STRAINS)))) \
	 $(PRJ_AS)_in.454.fastq \
	 $(PRJ_AS)_straindata_in.txt \
	 $(PRJ_AS)_traceinfo_in.454.xml \
	 step1_assembly.log \
	 step2_assembly.log \
	 step3_assembly.log
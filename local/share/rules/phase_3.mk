### phase_1.mk --- 
## 
## Filename: phase_1.mk
## Description: 
## Author: Michele
## Maintainer: 
## Created: Tue Jan 22 09:19:29 2013 (+0100)
## Version: 
## Last-Updated: 
##           By: 
##     Update #: 0
## URL: 
## Keywords: 
## Compatibility: 
## 
######################################################################
## 
### Commentary: 
## 
## 
## 
######################################################################
## 
### Change Log:
## 
## 
######################################################################
## 
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License as
## published by the Free Software Foundation; either version 3, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
## General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program; see the file COPYING.  If not, write to
## the Free Software Foundation, Inc., 51 Franklin Street, Fifth
## Floor, Boston, MA 02110-1301, USA.
## 
######################################################################
## 
### Code:


# assemblies logs
extern ../phase_1/step1_assembly.log as STEP1_ASSEMBLY_LOG
extern ../phase_1/step2_assembly.log as STEP2_ASSEMBLY_LOG
extern ../phase_1/step3_assembly.log as STEP3_ASSEMBLY_LOG

# contig read list files
extern ../phase_1/step1_assembly/step1_d_info/step1_info_contigreadlist.txt as STEP1_INFOCONTIGREADLIST

extern ../phase_1/step2_CDNA1_assembly/step2_CDNA1_d_info/step2_CDNA1_info_contigreadlist.txt as STEP2_CDNA1_INFOCONTIGREADLIST
extern ../phase_1/step2_CDNA2_assembly/step2_CDNA2_d_info/step2_CDNA2_info_contigreadlist.txt as STEP2_CDNA2_INFOCONTIGREADLIST
extern ../phase_1/step2_CDNA3_assembly/step2_CDNA3_d_info/step2_CDNA3_info_contigreadlist.txt as STEP2_CDNA3_INFOCONTIGREADLIST
extern ../phase_1/step2_CDNA4_assembly/step2_CDNA4_d_info/step2_CDNA4_info_contigreadlist.txt as STEP2_CDNA4_INFOCONTIGREADLIST

extern ../phase_1/step2_remain_assembly/step2_remain_d_info/step2_remain_info_contigreadlist.txt as STEP2_REMAINS_INFOCONTIGREADLIST


# debris
extern ../phase_1/step1_assembly/step1_d_info/step1_info_debrislist.txt as STEP1_DEBRISLIST

extern ../phase_1/step2_CDNA1_assembly/step2_CDNA1_d_info/step2_CDNA1_info_debrislist.txt as STEP2_CDNA1_DEBRISLIST
extern ../phase_1/step2_CDNA2_assembly/step2_CDNA2_d_info/step2_CDNA2_info_debrislist.txt as STEP2_CDNA2_DEBRISLIST
extern ../phase_1/step2_CDNA3_assembly/step2_CDNA3_d_info/step2_CDNA3_info_debrislist.txt as STEP2_CDNA3_DEBRISLIST
extern ../phase_1/step2_CDNA4_assembly/step2_CDNA4_d_info/step2_CDNA4_info_debrislist.txt as STEP2_CDNA4_DEBRISLIST

extern ../phase_1/step2_remain_assembly/step2_remain_d_info/step2_remain_info_debrislist.txt as STEP2_REMAIN_DEBRISLIST








# extract parameters from logs
step1_assembly.log.param: $(STEP1_ASSEMBLY_LOG)
	bawk '/Parameter settings seen for:/,/^--------*/' $< >$@   * extract text between two regular expression  *

step2_assembly.log.param: $(STEP2_ASSEMBLY_LOG)
	bawk '/Parameter settings seen for:/,/^--------*/' $< >$@

# results empty
step3_assembly.log.param: $(STEP3_ASSEMBLY_LOG)
	bawk '/Parameter settings seen for:/,/^--------*/' $< >$@

.IGNORE: step1-2_assembly.log.param.diff
step1-2_assembly.log.param.diff: step1_assembly.log.param step2_assembly.log.param
	diff \
	--side-by-side \
	--ignore-case \
	--ignore-tab-expansion \
	--ignore-trailing-space \
	--ignore-all-space \
	--ignore-blank-lines \
	$< $^2 >$@


# contigreadlist reads in step 2, which do not appear in step 1 contigreadlist
.SECONDEXPANSION: step1-step2-CDNA%_contigreadlist.diff   * expland variables at the second expansion *
step1-2-CDNA%_contigreadlist.diff: $$(STEP1_INFOCONTIGREADLIST) $$(STEP2_CDNA%_INFOCONTIGREADLIST)   * first expand %, then expand variables *
	comm -1 -3 \
	<(bawk '!/^[$$,\#+]/ { print $$2; }' <$< | bsort) \
	<(bawk '!/^[$$,\#+]/ { print $$2; }' <$^2 | bsort) \
	> $@

# reads placed in the debris in step 1, and assembled in step 2
.SECONDEXPANSION: step1-2-CDNA%_debrislist-contigreadlist.diff
step1-2-CDNA%_debrislist-contigreadlist.comm: $$(STEP1_DEBRISLIST) $$(STEP2_CDNA%_INFOCONTIGREADLIST)
	comm -1 -2 \
	<(bsort $<) \
	<(bawk '!/^[$$,\#+]/ { print $$2; }' <$^2 | bsort) \
	> $@


# reads in step2_remain_info_contigreadlist.txt that also appears in other assemblies of step 2
.SECONDEXPANSION: step2-CDNA%-remain_contigreadlist.comm
step2-CDNA%-remain_contigreadlist.comm: $$(STEP2_REMAINS_INFOCONTIGREADLIST) $$(STEP2_CDNA%_INFOCONTIGREADLIST)
	comm -1 -2 \
	<(bawk '!/^[$$,\#+]/ { print $$2; }' <$< | bsort) \
	<(bawk '!/^[$$,\#+]/ { print $$2; }' <$^2 | bsort) \
	> $@




CDNA%_remain_contig-debris.lst: $(STEP2_REMAINS_INFOCONTIGREADLIST) $(STEP2_REMAIN_DEBRISLIST)
	cat <(bawk '!/^[$$,\#+]/ { \
	if ($$2 ~ /^CDNA$*/) {print $$2}; \
	}' <$<) \
	<(bawk '!/^[$$,\#+]/ { \
	if ($$1 ~ /^CDNA$*/) {print $$1}; \
	}' <$^2) \
	| bsort \
	| uniq > $@


.SECONDEXPANSION: step2-CDNA%_debris-remain_contigreadlist.comm-diff
step2-CDNA%_debris-remain_contig-debris.diff: $$(STEP2_CDNA%_DEBRISLIST) CDNA%_remain_contig-debris.lst
	comm --check-order -2 -3 \
	<(bsort $<) \
	<(bsort $^2) \
	> $@



.SECONDEXPANSION: step2-CDNA%_debris-remain_contigreadlist.comm-diff
step2-CDNA%_debris-remain_contig-debris.diff: $$(STEP2_CDNA%_DEBRISLIST) CDNA%_remain_contig-debris.lst
	comm --check-order -2 -3 \
	<(bsort $<) \
	<(bsort $^2) \
	> $@




.SECONDEXPANSION: step1_debris-step2_remain_contig-debris.diff
step1_debris-step2-CDNA%_remain_contig-debris.diff: $$(STEP1_DEBRISLIST) CDNA%_remain_contig-debris.lst
	comm --check-order-2 -3 \
	<(bsort $<) \
	<(bsort $^2) \
	> $@








.PHONY: test
test:
	@echo $(454_INPUT)


# This should be the default target.
ALL   += 





# The dependency of the targhet listened here
# are treated as intermediate files. So they 
# they are automatically erased later on 
# after they are no longer needed. 
INTERMEDIATE +=






# Delete all files in the current directory 
# that are normally created by building the program.
CLEAN += 


######################################################################
### phase_1.mk ends here

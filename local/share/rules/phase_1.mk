### phase_1.mk --- 
## 
## Filename: phase_1.mk
## Description: 
## Author: Michele
## Maintainer: 
## Created: Tue Jan 22 09:19:29 2013 (+0100)
## Version: 
## Last-Updated: 
##           By: 
##     Update #: 0
## URL: 
## Keywords: 
## Compatibility: 
## 
######################################################################
## 
### Commentary: 
## 
## 
## 
######################################################################
## 
### Change Log:
## 
## 
######################################################################
## 
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License as
## published by the Free Software Foundation; either version 3, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
## General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program; see the file COPYING.  If not, write to
## the Free Software Foundation, Inc., 51 Franklin Street, Fifth
## Floor, Boston, MA 02110-1301, USA.
## 
######################################################################
## 
### Code:

PRJ1 ?= test
PRJ2 ?= stellatus
PRJ_AS ?= test1
MIRA_ADD_PARAMS ?= -OUT:sssip=yes \   * include singlets *

context prj/454_preprocessing

# naccarii CDNA3-CDNA4
extern ../../../../454_preprocessing/dataset/$(PRJ1)/phase_2/cleaned.fasta as FASTA_STR1
extern ../../../../454_preprocessing/dataset/$(PRJ1)/phase_2/cleaned.qual as QUAL_STR1
extern ../../../../454_preprocessing/dataset/$(PRJ1)/phase_2/cleaned.strain as STRAIN_STR1
extern ../../../../454_preprocessing/dataset/$(PRJ1)/phase_2/cleaned.xml as XML_STR1

# stellatus CDNA1-CDNA2
extern ../../../../454_preprocessing/dataset/$(PRJ2)/phase_2/cleaned.fasta as FASTA_STR2
extern ../../../../454_preprocessing/dataset/$(PRJ2)/phase_2/cleaned.qual as QUAL_STR2
extern ../../../../454_preprocessing/dataset/$(PRJ2)/phase_2/cleaned.strain as STRAIN_STR2
extern ../../../../454_preprocessing/dataset/$(PRJ2)/phase_2/cleaned.xml as XML_STR2


# links
strain1.fasta:
	ln -sf $(FASTA_STR1) $@

strain1.qual:
	ln -sf $(QUAL_STR1) $@

strain1.strain:
	ln -sf $(STRAIN_STR1) $@

strain1.xml:
	ln -sf $(XML_STR1) $@

strain2.fasta:
	ln -sf $(FASTA_STR2) $@

strain2.qual:
	ln -sf $(QUAL_STR2) $@

strain2.strain:
	ln -sf $(STRAIN_STR2) $@

strain2.xml:
	ln -sf $(XML_STR2) $@


# prepare MIRA input files
$(PRJ_AS)_in.454.fasta: strain1.fasta strain2.fasta
	cat $< $^2 > $@
$(PRJ_AS)_in.454.fasta.qual: strain1.qual strain2.qual
	cat $< $^2 > $@
$(PRJ_AS)_straindata_in.txt: strain1.strain strain2.strain
	cat $< $^2 > $@
$(PRJ_AS)_traceinfo_in.454.xml: strain1.xml strain2.xml
	merge_trace_xmls $< $^2 -o $@

# original parameters
#/home/michele/Research/Projects/Transcriptomic_analysis/Src_real//local/bin/mira_3.2.1_prod_linux-gnu_x86_64_static/bin/mira -job=denovo,est,accurate,454 -fasta -project=CDNA3-4_11_2010_0 COMMON_SETTINGS -GE:not=4 -LR:fo=no -SB:lsd=yes 454_SETTINGS -CL:cpat=0:qc=0 -ED:ace=1 -OUT:sssip=yes -CO:fnicpst=yes -LR:mxti=yes -AS:mrpc=1

# avoid parallel execution targets
.NOTPARALLEL: step1_assembly.log step2_assembly.log step3_assembly.log


# assembly
.PRECIOUS: step1_assembly.log
step1_assembly.log: $(PRJ_AS)_in.454.fasta $(PRJ_AS)_in.454.fasta.qual $(PRJ_AS)_straindata_in.txt $(PRJ_AS)_traceinfo_in.454.xml
	!threads
	miraSearchESTSNPs --project=$(PRJ_AS) \
	--job=denovo,accurate,454,esps1 -fasta \   * esps1 means step 1 *
	COMMON_SETTINGS \
	-GE:not=$$THREADNUM \   * use N cpus *
	-SB:lsd=yes \   * load straindata file *
	-LR:mxti=yes \   * load XML trace info file *
	-OUT:ora=yes \   * include singlets *
	454_SETTINGS \
	-CL:qc=no:cpat=no \   * no quality clippin, no polyA masking *
	-CO:fnicpst=yes \   * force non-IUPAC consensus *
	-ED:ace=yes \   * produce ACE file *
	-AS:mrpc=1 \   * min number of reads in a contig *
	$(MIRA_ADD_PARAMS) \
	>&$@

.PRECIOUS: step2_assembly.log
step2_assembly.log: step1_assembly.log
	!threads
	miraSearchESTSNPs \
	--job=denovo,accurate,454,esps2 \   * esps1 means step 1 *
	COMMON_SETTINGS \
	-GE:not=$$THREADNUM \   * use N cpus *
	-OUT:ora=yes \   * produce ace file *
	454_SETTINGS \
	-CL:qc=no:cpat=no \   * no quality clippin, no polyA masking *
	-CO:fnicpst=yes \   * force non-IUPAC consensus *
	-ED:ace=yes \   * produce ACE file *
	-AS:mrpc=1 \   * min number of reads in a contig *
	$(MIRA_ADD_PARAMS) \
	>&$@


.PRECIOUS: step3_assembly.log
step3_assembly.log: step2_assembly.log
	!threads
	miraSearchESTSNPs \
	--job=denovo,accurate,454,esps3 \   * esps1 means step 1 *
	COMMON_SETTINGS \
	-GE:not=$$THREADNUM \   * use N cpus *
	-OUT:ora=yes \   * produce ace file *
	454_SETTINGS \
	-CL:qc=no:cpat=no \   * no quality clippin, no polyA masking *
	-CO:fnicpst=yes \   * force non-IUPAC consensus *
	-ED:ace=yes \   * produce ACE file *
	-AS:mrpc=1 \   * min number of reads in a contig *
	$(MIRA_ADD_PARAMS) \
	>&$@



step2_%_contigreadlist: step2_assembly.log
	bawk '!/^[$$,\#+]/ { \
	print $$0; \
	}' < step2_$*_assembly/step2_$*_d_info/step2_$*_info_contigreadlist.txt \
	| cut -f1 \
	| bsort --key=1,1 \
	| uniq -c \
	| tr -s [:blank:] \\t \
	| bawk '!/^[$$,\#+]/ { \
	sub(/step[0-9]_/, "", $$3); \
	print $$3, $$2; \
	}' > $@



%_contigreadlist_sub: %_assembly.log step2_CDNA1_contigreadlist
	bawk '!/^[$$,\#+]/ { \
	print $$0; \
	}' < $(basename $<)/$*_d_info/$*_info_contigreadlist.txt \
	| bsort --key=1,1 --key=2,2 \
	| translate -v $^2 2 > $@






%_info_contigreadlist_collapsed: %_assembly.log
	bawk '!/^[$$,\#+]/ { \
	print $$0; \
	}' < $(basename $<)/$*_d_info/$*_info_contigreadlist.txt \
	| bsort --key=1,1 --key=2,2 \
	| set_collapse 2 -s -o \
	> $@


%_info_contigreadlist_collapsed: %_assembly.log
	bawk '!/^[$$,\#+]/ { \
	print $$0; \
	}' < $(basename $<)/$*_d_info/$*_info_contigreadlist.txt \
	| bsort --key=1,1 --key=2,2 \
	| set_collapse 2 -s -o \
	> $@





%_allele_count: %_info_contigreadlist_collapsed
	bawk 'BEGIN { print "# sequence","CDNA1","CDNA2","CDNA3","CDNA4","remain"  } \
	!/^[$$,\#+]/ { \
	strain1=0; strain2=0; strain3=0; strain4=0; remain=0;\
	printf "%s\t",$$1; \
	split($$2,reads,";"); \
	for (i in reads) \
	{ \
	if (reads[i] ~ /CDNA1/) {strain1++}; \
	if (reads[i] ~ /CDNA2/) {strain2++}; \
	if (reads[i] ~ /CDNA3/) {strain3++}; \
	if (reads[i] ~ /CDNA4/) {strain4++}; \
	if (reads[i] ~ /remain/) {remain++}; \
	}; \
	printf "%i\t%i\t%i\t%i\t%i\n",strain1,strain2,strain3,strain4,remain; \
	}' <$< >$@


%_allele_count_boxplot.pdf: %_allele_count
	cut -f 6 --complement $< \
	| boxplot -r -o $@

%_allele_count_histo.pdf: %_allele_count
	distrib_plot -t histogram -b 21 -o $@ 2 3 4 5 <$<

# custom
%_allele_count_histo_lim.pdf: %_allele_count
	this_distrib_plot -r -t histogram -b 21 -o $@ 2 3 4 5 <$<


%_allele_shared: %_allele_count
	bawk 'BEGIN { print "# sequence","CDNA1","CDNA2","CDNA3","CDNA4","remain"  } \
	!/^[$$,\#+]/ { \
	if ( $$2 > 0 && $$3 > 0 && $$4 > 0 && $$5 > 0 ) \
	{ \
	print $$0; \
	} \
	}' <$< >$@


%_allele_double: %_allele_count
	bawk 'BEGIN { print "# sequence","CDNA1","CDNA2","CDNA3","CDNA4","remain"  } \
	!/^[$$,\#+]/ { \
	if ( ( $$4 >= 2*$$2 && $$5 >= 2*$$3 ) && ( $$4 >= 2*$$3 && $$5 >= 2*$$2 ) ) \
	{ \
	print $$0; \
	} \
	}' <$< >$@


%_allele_shared_double: %_allele_count
	bawk 'BEGIN { print "# sequence","CDNA1","CDNA2","CDNA3","CDNA4","remain"  } \
	!/^[$$,\#+]/ { \
	if ( ( $$4 >= 2*$$2 && $$5 >= 2*$$3 ) && ( $$4 >= 2*$$3 && $$5 >= 2*$$2 ) ) \
	{ \
	print $$0; \
	} \
	}' <$< >$@



%_allele_max: %_allele_count
	bawk 'BEGIN { print "# sequence","CDNA1-2","CDNA3-4"  } \
	function max(x){i=0;for(val in x){if(i<=x[val]){i=x[val];}}return i;} \
	function min(x){i=max(x);for(val in x){if(i>x[val]){i=x[val];}}return i;} \
	!/^[$$,\#+]/ { \
	a[1]=$$2; a[2]=$$3; b[1]=$$4; b[2]=$$5; \
	print $$1, max(a), max(b); \
	}' <$< > $@


%_allele_max_histo.pdf: %_allele_max
	distrib_plot -r -t histogram -b 21 -o $@ 2 3 <$<


%_allele_ratio: %_allele_max
	bawk 'BEGIN { print "# sequence","CDNA1-2","CDNA3-4"; up1=0; low1=0; eq1=0;  } \
	!/^[$$,\#+]/ { \
	if ( $$2 > 0 && $$3 > 0 ) { \
	print $$1, $$3/$$2; \
	} \
	}' <$< > $@


%_allele_bins: %_allele_max
	>$@; \
	for BIN in {1..8}; do \
	bawk -v bin=$$BIN 'BEGIN { print "# bin",">"bin,"<"bin,"="bin; up1=0; low1=0; eq1=0;  } \
	!/^[$$,\#+]/ { \
	if ( $$2 > 0 && $$3 > 0 ) { \
	ratio=($$3)/($$2); \
	a=bin; b=1/a; \
	if ( ratio > a ) {up1++;} \
	if ( ratio < b ) {low1++;} \
	if ( ratio >= b && ratio <= a ) {eq1++;} \
	} \
	} END { print bin,up1,low1,eq1;  }' <$< >> $@; \
	done



%_allele_ratio_histo.pdf: %_allele_ratio
	distrib_plot -r -t histogram -b 8 -o $@ 2 <$<


%_allele_ratio_boxplot.pdf: %_allele_ratio
	sed '1d' $< | boxplot -e -r -o $@







.PHONY: test
test:
	@echo $(454_INPUT)


# This should be the default target.
ALL   += $(PRJ_AS)_in.454.fasta \
	 $(PRJ_AS)_in.454.fasta.qual \
	 $(PRJ_AS)_straindata_in.txt \
	 $(PRJ_AS)_traceinfo_in.454.xml \
	 step1_assembly.log \
	 step2_assembly.log \
	 step3_assembly.log \






# The dependency of the targhet listened here
# are treated as intermediate files. So they 
# they are automatically erased later on 
# after they are no longer needed. 
INTERMEDIATE +=






# Delete all files in the current directory 
# that are normally created by building the program.
CLEAN += strain1.fasta \
	 strain1.qual \
	 strain1.strain \
	 strain1.xml \
	 strain2.fasta \
	 strain2.qual \
	 strain2.strain \
	 strain2.xml \
	 step1_assembly.log \
	 step2_assembly.log \
	 step3_assembly.log



######################################################################
### phase_1.mk ends here
